```sh
npm install --location=global pnpm
pnpm install
pnpm dev # start development server
pnpm build # build for production
pnpm start # start production server
```

### Missing

1. Generate HTML from Handlebars Templates
2. Better SCSS Handling

I want to add Gulp to handle those