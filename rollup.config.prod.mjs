import babel from '@rollup/plugin-babel';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import eslint from '@rollup/plugin-eslint';
import terser from '@rollup/plugin-terser';

import scss from 'rollup-plugin-scss';
import sass from 'sass';
import postcss from 'postcss';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';

import path from 'node:path';
import { fileURLToPath } from 'node:url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
/* eslint-disable */
global['__filename'] = __filename;
global['__dirname'] = __dirname;

const plugins = [
  resolve({
    moduleDirectories: ['node_modules']
  }),
  commonjs({
    include: ['node_modules/**']
  }),
  eslint({
    exclude: ['src/styles/**'],
  }),
  babel({
    babelHelpers: 'bundled',
    exclude: 'node_modules/**'
  })
]

export default [
  {
    input: 'src/main.js',
    output: {
      file: 'dist/app.js',
      format: 'iife',
      assetFileNames: '[name][extname]',
      sourcemap: false,
      globals: {
        lodash: '_',
        jquery: '$'
      }
    },
    external: ['lodash','jquery'],
    plugins: [
      scss({
        fileName: 'main.css',
        failOnError: true,
        sass: sass,
        processor: async (css) => {
          const result = await postcss([autoprefixer({
            add: true,
            flexbox: true,
            grid: true
          })])
            .process(css,{from: undefined})
          return result.css
        }
      }),
      ...plugins
    ]
  },
  {
    input: 'src/vendor.js',
    output: {
      file: 'dist/vendor.js',
      format: 'es',
      assetFileNames: '[name][extname]',
      sourcemap: false
    },
    plugins: [
      scss({
        fileName: 'vendor.css',
        failOnError: true,
        sass: sass,
        processor: async (css) => {
          const result = await postcss([cssnano,autoprefixer({
            add: true,
            flexbox: true,
            grid: true
          })])
            .process(css,{from: undefined})
          return result.css
        }
      }),
      ...plugins,
      terser()
    ]
  },
];