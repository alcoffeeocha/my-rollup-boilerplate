/* eslint-disable */
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import eslint from '@rollup/plugin-eslint';
import browsersync from 'rollup-plugin-browsersync';

import path from 'node:path';
import { fileURLToPath } from 'node:url';
import * as dotenv from 'dotenv';
import terser from '@rollup/plugin-terser';

dotenv.config()
const { DEV_PORT } = process.env;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
global['__filename'] = __filename;
global['__dirname'] = __dirname;

const plugins = [
  resolve({
    moduleDirectories: ['node_modules']
  }),
  commonjs({
    include: ['node_modules/**']
  }),
  eslint({
    exclude: ['src/styles/**'],
  })
]


export default [
  {
    input: 'src/vendor.js',
    output: {
      file: 'public/vendor.js',
      assetFileNames: '[name][extname]',
      format: 'es'
    },
    plugins: [
      ...plugins,
      terser(),
    ],
    watch: {
      exclude: ['src/vendor.js']
    }
  },
  {
    input: 'src/main.js',
    output: {
      file: 'public/app.js',
      format: 'iife',
      assetFileNames: '[name][extname]',
      sourcemap: 'inline',
      globals: {
        lodash: '_',
        jquery: '$'
      }
    },
    external: ['lodash','jquery'],
    plugins: [
      ...plugins,
      browsersync({
        port: DEV_PORT,
        server: "public",
        watch: true,
        open: 'local',
        notify: false
      })
    ]
  }
];